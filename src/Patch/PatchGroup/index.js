import React, { useState } from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import PatchField from "../PatchField";
import AddCropButton from "../AddCropButton";
import getBottomRightSelectedCell from "./getBottomRightSelectedCell";

const useStyles = makeStyles(() => ({
  root: {
    position: "relative",
    top: 40,
    left: 40,
    backgroundColor: "#ccb59a",
    padding: 6,
    width: "fit-content",
  },
  row: {
    display: "flex",
    flexDirection: "row",
  },
}));

const PatchGroup = ({ patch }) => {
  const classes = useStyles();
  const [fields, setFields] = useState(() => {
    const fields = {};
    for (let column = -1; column < patch.amountCols + 1; column++) {
      for (let row = -1; row < patch.amountRows + 1; row++) {
        fields[`${column}${row}`] = {
          fieldType: "empty", // fieldType can be 'empty', 'selected' or a crop's id
          freeUntil: Math.random() < 0.3 ? `02.22` : null,
        };
      }
    }

    return fields;
  });

  const selectField = (x, y, fieldType) => {
    setFields({
      ...fields,
      [`${x}${y}`]: {
        ...fields[`${x}${y}`],
        fieldType,
      },
    });
  };

  const plantCrops = (cropId) => {
    const updatedFields = Object.entries(fields).reduce(
      (accumulator, [key, field]) => ({
        ...accumulator,
        [key]: {
          ...fields[key],
          ...(field.fieldType === "selected" && { fieldType: cropId }),
        },
      }),
      {}
    );

    setFields(updatedFields);
  };

  const [buttonX, buttonY] = getBottomRightSelectedCell(fields);

  return (
    <div className={classes.root}>
      {Array(patch.amountRows)
        .fill(null)
        .map((_, rowIndex) => (
          <div className={classes.row} key={rowIndex}>
            {Array(patch.amountCols)
              .fill(null)
              .map((_, columnIndex) => (
                <PatchField
                  key={columnIndex}
                  rowIndex={rowIndex}
                  columnIndex={columnIndex}
                  selectField={selectField}
                  fieldData={fields[`${columnIndex}${rowIndex}`]}
                  topNeighbour={fields[`${columnIndex}${rowIndex - 1}`]}
                  topRightNeighbour={
                    fields[`${columnIndex + 1}${rowIndex - 1}`]
                  }
                  rightNeighbour={fields[`${columnIndex + 1}${rowIndex}`]}
                  bottomRightNeighbour={
                    fields[`${columnIndex + 1}${rowIndex + 1}`]
                  }
                  bottomNeighbour={fields[`${columnIndex}${rowIndex + 1}`]}
                  bottomLeftNeighbour={
                    fields[`${columnIndex - 1}${rowIndex + 1}`]
                  }
                  leftNeighbour={fields[`${columnIndex - 1}${rowIndex}`]}
                  topLeftNeighbour={fields[`${columnIndex - 1}${rowIndex - 1}`]}
                />
              ))}
          </div>
        ))}
      {(!!buttonX || buttonX === 0) && (!!buttonY || buttonY === 0) && (
        <AddCropButton ton x={buttonX} y={buttonY} plantCrops={plantCrops} />
      )}
    </div>
  );
};

export default PatchGroup;
