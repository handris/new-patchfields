const getBottomRightSelectedCell = (fields) => {
  // we are looking for the largest x coordinate (rightmost cell) and largest y coordinate (bottommost cell)

  const coordinates = Object.entries(fields).reduce(
    (accumulator, [currentFieldCoordinates, fieldProperties]) => {
      const [cellX, cellY] = currentFieldCoordinates.split("").map(Number);

      // return in case of negative numbers, they are not needed, they are only a convenience
      if (isNaN(cellX) || isNaN(cellY)) {
        return accumulator;
      }

      if (
        fieldProperties.fieldType === "selected" &&
        (accumulator.x === null || accumulator.x < cellX)
      ) {
        accumulator.x = cellX;
      }

      if (
        fieldProperties.fieldType === "selected" &&
        (accumulator.y === null || accumulator.y < cellY)
      ) {
        accumulator.y = cellY;
      }

      return accumulator;
    },
    { x: null, y: null }
  );

  return [coordinates.x, coordinates.y];
};

export default getBottomRightSelectedCell;
