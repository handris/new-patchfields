import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import clsx from "clsx";
import { borderSize, patchFieldSize } from "../sizes";
import FreeUntilLabel from "../FreeUntilLabel";
import PatchFieldImage from "./PatchFieldImage";
import PatchFieldCorners from "./PatchFieldCorners";

const useStyles = makeStyles(() => ({
  outer: {
    backgroundColor: "#ccb59a",
    position: "relative",
    width: patchFieldSize + 2 * borderSize,
    height: patchFieldSize + 2 * borderSize,
    cursor: "pointer",
  },
  inner: {
    boxSizing: "border-box",
    width: patchFieldSize,
    height: patchFieldSize,
    backgroundColor: "#fff",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: borderSize,
    left: borderSize,
  },
  selectedOuter: {
    backgroundColor: "#56434e",
  },
  selectedInner: {
    width: ({ horizontalNeighbourCount }) =>
      patchFieldSize + horizontalNeighbourCount * borderSize,
    left: ({ hasSameTypeLeftNeighbour }) =>
      hasSameTypeLeftNeighbour ? 0 : borderSize,
    height: ({ verticalNeighbourCount }) =>
      patchFieldSize + verticalNeighbourCount * borderSize,
    top: ({ hasSameTypeTopNeighbour }) =>
      hasSameTypeTopNeighbour ? 0 : borderSize,
  },
}));

const PatchField = ({
  rowIndex,
  columnIndex,
  selectField,
  fieldData,
  topNeighbour,
  topRightNeighbour,
  rightNeighbour,
  bottomRightNeighbour,
  bottomNeighbour,
  bottomLeftNeighbour,
  leftNeighbour,
  topLeftNeighbour,
}) => {
  const { freeUntil, fieldType } = fieldData;

  const horizontalNeighbourCount =
    (leftNeighbour.fieldType === fieldType && fieldType !== "empty" ? 1 : 0) +
    (rightNeighbour.fieldType === fieldType && fieldType !== "empty" ? 1 : 0);
  const verticalNeighbourCount =
    (topNeighbour.fieldType === fieldType && fieldType !== "empty" ? 1 : 0) +
    (bottomNeighbour.fieldType === fieldType && fieldType !== "empty" ? 1 : 0);

  const classes = useStyles({
    horizontalNeighbourCount,
    verticalNeighbourCount,
    hasSameTypeTopNeighbour: topNeighbour.fieldType === fieldType,
    hasSameTypeLeftNeighbour: leftNeighbour.fieldType === fieldType,
  });

  return (
    <div
      className={clsx(
        classes.outer,
        fieldType === "selected" && classes.selectedOuter
      )}
      onClick={() =>
        selectField(
          columnIndex,
          rowIndex,
          fieldType === "empty" || typeof fieldType === "number"
            ? "selected"
            : "empty"
        )
      }
    >
      <div
        className={clsx(
          classes.inner,
          fieldType !== "empty" && classes.selectedInner
        )}
      >
        {fieldType === "empty" && freeUntil && (
          <FreeUntilLabel freeUntil={freeUntil} />
        )}
      </div>

      <PatchFieldImage fieldType={fieldType} />

      <PatchFieldCorners
        fieldType={fieldType}
        topNeighbour={topNeighbour}
        topRightNeighbour={topRightNeighbour}
        rightNeighbour={rightNeighbour}
        bottomRightNeighbour={bottomRightNeighbour}
        bottomNeighbour={bottomNeighbour}
        bottomLeftNeighbour={bottomLeftNeighbour}
        leftNeighbour={leftNeighbour}
        topLeftNeighbour={topLeftNeighbour}
      />
    </div>
  );
};

export default PatchField;
