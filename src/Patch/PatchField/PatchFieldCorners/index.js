import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import clsx from "clsx";
import { borderSize } from "../../sizes";

const useStyles = makeStyles(() => ({
  corner: {
    width: borderSize,
    height: borderSize,
    position: "absolute",
  },
  selectedCorner: {
    backgroundColor: "#56434e",
  },
  cropCorner: {
    backgroundColor: "#ccb59a",
  },
  bottomRightCorner: {
    bottom: 0,
    right: 0,
  },
  bottomLeftCorner: {
    bottom: 0,
    left: 0,
  },
  topLeftCorner: {
    top: 0,
    left: 0,
  },
  topRightCorner: {
    top: 0,
    right: 0,
  },
}));

const PatchFieldCorners = ({
  fieldType,
  topNeighbour,
  topRightNeighbour,
  rightNeighbour,
  bottomRightNeighbour,
  bottomNeighbour,
  bottomLeftNeighbour,
  leftNeighbour,
  topLeftNeighbour,
}) => {
  const classes = useStyles();

  const hasBottomRightCorner =
    fieldType !== "empty" &&
    rightNeighbour.fieldType === fieldType &&
    bottomNeighbour.fieldType === fieldType &&
    bottomRightNeighbour.fieldType !== fieldType;

  const hasBottomLeftCorner =
    fieldType !== "empty" &&
    leftNeighbour.fieldType === fieldType &&
    bottomNeighbour.fieldType === fieldType &&
    bottomLeftNeighbour.fieldType !== fieldType;

  const hasTopRightCorner =
    fieldType !== "empty" &&
    rightNeighbour.fieldType === fieldType &&
    topNeighbour.fieldType === fieldType &&
    topRightNeighbour.fieldType !== fieldType;

  const hasTopLeftCorner =
    fieldType !== "empty" &&
    leftNeighbour.fieldType === fieldType &&
    topNeighbour.fieldType === fieldType &&
    topLeftNeighbour.fieldType !== fieldType;

  return (
    <>
      {hasBottomRightCorner && (
        <div
          className={clsx(
            classes.corner,
            classes.bottomRightCorner,
            fieldType === "selected"
              ? classes.selectedCorner
              : classes.cropCorner
          )}
        />
      )}
      {hasBottomLeftCorner && (
        <div
          className={clsx(
            classes.corner,
            classes.bottomLeftCorner,
            fieldType === "selected"
              ? classes.selectedCorner
              : classes.cropCorner
          )}
        />
      )}
      {hasTopLeftCorner && (
        <div
          className={clsx(
            classes.corner,
            classes.topLeftCorner,
            fieldType === "selected"
              ? classes.selectedCorner
              : classes.cropCorner
          )}
        />
      )}
      {hasTopRightCorner && (
        <div
          className={clsx(
            classes.corner,
            classes.topRightCorner,
            fieldType === "selected"
              ? classes.selectedCorner
              : classes.cropCorner
          )}
        />
      )}
    </>
  );
};

export default PatchFieldCorners;
