import React from "react";
import { crops } from "../../../App";
import clsx from "clsx";
import { patchFieldSize } from "../../sizes";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles(() => ({
  cropImage: {
    maxHeight: patchFieldSize * 0.8,
    maxWidth: patchFieldSize * 0.8,
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  icon: {
    fontSize: 20,
    color: "#13905b",
    position: "absolute",
    zIndex: 2,
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
}));

const PatchFieldImage = ({ fieldType }) => {
  const classes = useStyles();
  return (
    <>
      {typeof fieldType === "number" && (
        <img
          className={classes.cropImage}
          src={crops.find((crop) => crop.id === fieldType).image}
          alt={"Crop"}
        />
      )}
      {fieldType === "empty" && (
        <i className={clsx("zwicon-plus-circle", classes.icon)} />
      )}
      {fieldType === "selected" && (
        <i className={clsx("zwicon-minus-circle", classes.icon)} />
      )}
    </>
  );
};

export default PatchFieldImage;
