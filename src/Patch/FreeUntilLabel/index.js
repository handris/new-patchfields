import React from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { patchFieldSize } from "../sizes";

const useStyles = makeStyles(() => ({
  root: {
    height: patchFieldSize / 4,
    color: "#fff",
    backgroundColor: "#13905b",
    fontSize: 12,
    position: "absolute",
    bottom: 0,
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    userSelect: "none",
  },
}));
const FreeUntilLabel = ({ freeUntil }) => {
  const classes = useStyles();

  return <div className={classes.root}>frei bis {freeUntil}</div>;
};

export default FreeUntilLabel;
