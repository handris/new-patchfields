import React, { useState } from "react";
import makeStyles from "@material-ui/core/styles/makeStyles";
import clsx from "clsx";
import { patchFieldSize } from "../sizes";
import { crops } from "../../App";

const useStyles = makeStyles(() => ({
  root: {
    cursor: "pointer",
    backgroundColor: "#13905b",
    width: 55,
    height: 55,
    borderRadius: "100%",
    filter: "drop-shadow(2px 3px 3px rgba(0, 0, 0, 0.3))",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: ({ y }) => (y + 1) * patchFieldSize,
    left: ({ x }) => (x + 1) * patchFieldSize,
  },
  icon: {
    fontSize: 36,
    color: "#fff",
  },
  cropChooser: {
    display: "flex",
    flexDirection: "row",
    position: "absolute",
    top: 55,
    "&>:first-child": {
      marginRight: 16,
    },
  },
  cropIcon: {
    width: 20,
    height: 20,
    borderRadius: "100%",
    display: "block",
    border: "3px solid transparent",
  },
  selectedCropIcon: {
    border: "2px solid #13905b",
  },
}));

const AddCropButton = ({ x, y, plantCrops }) => {
  const classes = useStyles({ x, y });
  const [selectedCropId, setSelectedCropId] = useState(1);

  return (
    <div className={classes.root} onClick={() => plantCrops(selectedCropId)}>
      <i className={clsx("zwicon-edit-pencil", classes.icon)} />
      <div className={classes.cropChooser}>
        {crops.map((crop) => (
          <img
            className={clsx(
              classes.cropIcon,
              crop.id === selectedCropId && classes.selectedCropIcon
            )}
            src={crop.image}
            key={crop.id}
            alt=""
            onClick={(event) => {
              event.stopPropagation();
              setSelectedCropId(crop.id);
            }}
          />
        ))}
      </div>
    </div>
  );
};

export default AddCropButton;
