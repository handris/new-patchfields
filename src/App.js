import React from "react";
import PatchGroup from './Patch/PatchGroup';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const crops = [
  { id: 1, image: "/images/beetroot.jpeg"},
  { id: 2, image: "/images/broadbean.jpeg"}
]

const useStyles = makeStyles(() => ({
  root: {
    backgroundColor: "#8c807f",
    height: "100vh",
  }
}))

function App() {
  const classes = useStyles();

  const patch =
    {
      amountCols: 4,
      amountRows: 4,
    }

  return (
    <div className={classes.root}>
      <PatchGroup patch={patch} />
    </div>
  );
}

export default App;
